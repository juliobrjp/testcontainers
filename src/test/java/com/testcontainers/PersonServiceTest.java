package com.testcontainers;

import java.util.List;
import com.testcontainers.entity.Address;
import com.testcontainers.entity.Person;
import com.testcontainers.service.PersonService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext
class PersonServiceTest {

	private final PersonService personService;

	@Autowired
	public PersonServiceTest(PersonService personService) {
		this.personService = personService;
	}

	@Test
	void testSave() {

		personService.save(getPerson());

		List<Person> result = personService.findAll();

		Assertions.assertEquals(1, result.size());
	}

	private static Person getPerson(){
		Person person = new Person();
		person.setFirstName("John");
		person.setLastName("Ziz");
		person.setActive(true);
		person.setAge(33);

		Address address = new Address();
		address.setStreet("Street Fighter");
		address.setCity("New York");
		address.setPostalCode("123456");
		address.setCountryCode("US");
		address.setState("NY");

		person.addAddress(address);
		return  person;
	}

}
