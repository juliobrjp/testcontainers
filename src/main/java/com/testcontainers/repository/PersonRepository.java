package com.testcontainers.repository;

import java.util.List;
import com.testcontainers.entity.Person;
import org.springframework.data.repository.Repository;

public interface PersonRepository extends Repository<Person, Integer> {

    Person save(Person person);

    List<Person> findAll();
}
