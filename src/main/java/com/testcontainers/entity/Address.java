package com.testcontainers.entity;

import javax.persistence.*;
import lombok.Data;

@Entity
@Data
public class Address {

    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name="address_sequence",sequenceName="address_seq")
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="address_sequence")
    private Long id;

    private String street;

    private String city;

    private String state;

    private String countryCode;

    private String postalCode;

    @ManyToOne
    private Person person;
}
