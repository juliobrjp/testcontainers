package com.testcontainers.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Entity
@Data
public class Person implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "person_sequence", sequenceName = "person_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "person_sequence")
    private Long id;

    private String firstName;

    private String lastName;

    private int age;

    private boolean active;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "person")
    private List<Address> addressList = new ArrayList<>();

    public void addAddress(Address address){
        address.setPerson(this);
        addressList.add(address);
    }
}
