package com.testcontainers.controller;

import com.testcontainers.entity.Person;
import com.testcontainers.service.PersonService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {

    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping(path= "/person/save", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> save(@RequestBody Person person){

        Person response = personService.save(person);

        return ResponseEntity.ok(response);
    }
}
